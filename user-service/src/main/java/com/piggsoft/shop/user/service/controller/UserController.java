/*
 *
 * Copyright (C) 1999-2016 IFLYTEK Inc.All Rights Reserved.
 * History：
 * Version   Author      Date                              Operation
 * 1.0       yaochen4    2018/6/21                           Create
 */
package com.piggsoft.shop.user.service.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author piggsoft
 * @version 1.0
 * @create 2018/6/21
 * @since 1.0
 */
@RestController
@RequestMapping("/")
public class UserController {

    @Value("${hello}")
    private String hello;

    @RequestMapping
    public String hello() {
        return hello;
    }

}
