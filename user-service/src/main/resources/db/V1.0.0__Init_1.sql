CREATE TABLE t_user (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY ,
  username VARCHAR(50) UNIQUE ,
  password VARCHAR(100) NOT NULL ,
  nick_name VARCHAR(50) NOT NULL ,
  email VARCHAR(100),
  phone VARCHAR(20) NOT NULL,
  INDEX phone_index (phone)
)